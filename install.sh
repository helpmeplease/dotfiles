#!/bin/bash

###
# This script moves dotfiles into the ~/dotfiles folders.
# It also creates some symlinks so that all config files are in .config
###

##################### variables
dir=~/dotfiles          # the directory of the dotfiles to install
backup_dir=~/dotfiles_old       # where to back up the old dotfiles    
# the full name of the files/folders you want to install, make sure to include the "." at the beginning
files=".bashrc .oh-my-zsh .zshrc .Xresources i3 nvim mpd polybar wallpapers termite"      
#####################

if [ -e $backup_dir ] && [ ! -d $backup_dir ];then
    echo -n "Please either change the backup directory in the script or delete the file named $backup_dir"
    exit 1
else
    mkdir $backup_dir
fi

# installs dotfiles

install_config(){
for file in $files; do
    echo "Moving $file to backup folder... "

    # moves the old config file if it is in .config and is either a directory or a file
    if [ -f ~/.config/$file ] ||  [ -d ~/.config/$file ];then
        mv ~/.config/$file $backup_dir
    fi

    # the same except it checks in ~
    if [ -f ~/$file ] || [ -d ~/$file ];then
        mv ~/$file $backup_dir
    fi

    echo "Installing $file and creating symlink ... "
    mv $dir/$file ~/.config/$file
    ln -s ~/.config/$file ~/$file

done
}

#installs tamzen font

install_tamzen(){
if [[ ! -d ~/.fonts/tamzen-font ]]; then
    git clone "https://github.com/sunaku/tamzen-font" ~/.fonts/tamzen-font
    xset +fp ~/.fonts/tamzen-font/bdf
    xset fp rehash
fi
}

# installs yay for arch users (btw)

install_yay(){
if [[ ! -e /bin/yay ]] && [[ -f /etc/arch-release ]]; then
    sudo pacman -S go
    git clone https://aur.archlinux.org/yay.git
    cd yay
    makepkg -si
fi
}

# disables mouse accel and sets mouse speed to 0 for raw input

install_mouseaccel_patch(){
if [[ ! -f /etc/X11/xorg.conf.d/50-mouse-acceleration.conf ]];then
    sudo touch /etc/X11/xorg.conf.d/50-mouse-acceleration.conf
    sudo echo "Section \"InputClass\"" >> /etc/X11/xorg.conf.d/50-mouse-acceleration.conf
    sudo echo " Identifier \"My Mouse\"" >> /etc/X11/xorg.conf.d/50-mouse-acceleration.conf
    sudo echo " Driver \"libinput\"" >> /etc/X11/xorg.conf.d/50-mouse-acceleration.conf
    sudo echo "	MatchIsPointer \"yes\"" >> /etc/X11/xorg.conf.d/50-mouse-acceleration.conf
    sudo echo "	Option \"AccelProfile\" \"flat\"" >> /etc/X11/xorg.conf.d/50-mouse-acceleration.conf
    sudo echo "	Option \"AccelSpeed\" \"0\""
    sudo echo "EndSection" >> /etc/X11/xorg.conf.d/50-mouse-acceleration.conf
fi
}

install_ohmyzsh(){
if [ ! -e ~/.oh-my-zsh];then
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
}

install_git(){
if [[ ! -e /bin/git ]] || [[ ! -e /usr/bin/git ]];then
    if [[ -f /etc/redhat-release ]]; then
            sudo yum install git
    elif [[ -f /etc/debian_version ]]; then
            sudo apt-get install git
    elif [[ -f /etc/arch-release ]]; then
            sudo pacman -S git
    else
        echo -n "Install git before running this script"
        exit 1
    fi
fi
}

install_tamzen

install_yay

install_mouseaccel_patch

install_ohmyzsh

install_git

install_config
