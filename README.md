# Dotfiles

My dotfiles, a collection of loosely glued together configurations from many people.

# Install

To install my dotfiles, run the install.sh script. You can also pick which packages you would like installed by changing the variables in the script. 

# Screenshots

![blank](blank.png)

![floating](floating.png)

![tiled](tiled.png)
